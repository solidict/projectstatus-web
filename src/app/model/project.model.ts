
export class Project {
  constructor(public active: boolean, public id: number, public name: string, public userId: number, public report: {Report}) {
    this.active = active;
    this.id = id;
    this.name = name;
    this.userId = userId;
    this.report = report;
  }
}
