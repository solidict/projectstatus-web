export class User {

  constructor(private id: number, private name: string, private email: string, public authToken: string) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.authToken = authToken;
  }

}
