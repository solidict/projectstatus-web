export class ActionResult {

  constructor(private message: string, private success: boolean) {
    this.message = message;
    this.success = success;
  }

}
