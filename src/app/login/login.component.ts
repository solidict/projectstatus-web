import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthServiceService} from "../auth-service/auth-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private authService: AuthServiceService) { }

  ngOnInit() {
  }

  loginForm(form: NgForm) {
    this.authService.login(form);
  }
}
