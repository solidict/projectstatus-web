import { Component, OnInit } from '@angular/core';
import {RequestOptions, Headers, Http} from "@angular/http";
import {Router} from "@angular/router";
import {UserService} from "../user/user.service";
import {Global} from "../global";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  projects = [];
  constructor(private router: Router, private http: Http, private global: Global, private  userService: UserService) { }

  ngOnInit() {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('x-projectstatus-token', localStorage.getItem('authToken'));
    const options = new RequestOptions();
    options.headers = myHeaders;
    this.http.get( this.global.baseUrl + "api/users/me", options).map((res) => res.json()).subscribe((data) => {
      this.projects = data.projects;
      console.log(this.projects);
    }, (error) => {

    });
  }


}
