import { BrowserModule } from '@angular/platform-browser';
import {NgModule, OnInit} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportsComponent } from './reports/reports.component';
import { ReportDetailComponent } from './reports/report-detail/report-detail.component';
import { ReportCreateComponent } from './reports/report-create/report-create.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectDetailComponent } from './projects/project-detail/project-detail.component';
import { ProjectCreateComponent } from './projects/project-create/project-create.component';
import { AuthGuardService } from "./auth-guard/auth-guard.service";
import { AuthServiceService } from "./auth-service/auth-service.service";
import { HeaderComponent } from './header/header.component';
import {HttpClientModule} from '@angular/common/http';
import {Global} from "./global";
import {UserService} from "./user/user.service";
import {CKEditorModule} from "ng2-ckeditor";
import { NotFoundComponent } from './not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuardService] },
  { path: 'projects', component: ProjectsComponent, canActivate: [AuthGuardService] },
  { path: 'projects/:id', component: ProjectDetailComponent, canActivate: [AuthGuardService], pathMatch: 'full' },
  { path: 'projects/:id/reports', component: ReportDetailComponent, canActivate: [AuthGuardService], pathMatch: 'full' },
  { path: 'projects/:id/reports/:rid', component: ReportCreateComponent, canActivate: [AuthGuardService], pathMatch: 'full' },
  { path: 'create/projects', component: ProjectCreateComponent, canActivate: [AuthGuardService] },
  { path: '**', component: NotFoundComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ReportsComponent,
    ReportDetailComponent,
    ReportCreateComponent,
    ProjectsComponent,
    ProjectDetailComponent,
    ProjectCreateComponent,
    HeaderComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    CKEditorModule
  ],
  providers: [
    AuthGuardService,
    AuthServiceService,
    UserService,
    Global
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
