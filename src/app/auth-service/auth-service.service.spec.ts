import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {Http, HttpModule} from "@angular/http";

import { AuthServiceService } from './auth-service.service';
import {Global} from "../global";
import {UserService} from "../user/user.service";

describe('AuthServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpModule],
      providers: [AuthServiceService, Global, UserService]
    });
  });

  it('should be created', inject([AuthServiceService], (service: AuthServiceService) => {
    expect(service).toBeTruthy();
  }));

  it('should be 200', inject([AuthServiceService], (service: AuthServiceService, done) => {
    let data;
    const test = service.checkConnection("omer.atas@turing.solidict.com", "omer2017!!!!").subscribe(
      (resp) => {
        data = resp;
        console.log(data);
      },
      (resp) => {
        data = resp;
        console.log(data);
        throw {
          name: "Error",
          message: "Parsing is not possible"
        };
      });

  }));

});
