import {Injectable, OnInit} from '@angular/core';
import { Router } from "@angular/router";
import { ActionResult } from "../model/actionResult.model";
import {Http, RequestOptions, Headers, RequestMethod, HttpModule} from "@angular/http";
import {Global} from "../global";
import {NgForm} from "@angular/forms";
import {UserService} from "../user/user.service";
import 'rxjs/add/operator/map';

@Injectable()
export class AuthServiceService {
  private actionResult: ActionResult;
  public loggedIn = false;

  constructor(private router: Router, private http: Http, private global: Global, private  userService: UserService) { }


  isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          if (localStorage.getItem('authToken')) {
            const myHeaders = new Headers();
            myHeaders.append('Content-Type', 'application/json');
            myHeaders.append('x-projectstatus-token', localStorage.getItem('authToken'));
            const options = new RequestOptions();
            options.headers = myHeaders;
            this.http.get( this.global.baseUrl + "api/users/me", options).map((res) => res.json()).subscribe((data) => {
              this.loggedIn = true;
              resolve(this.loggedIn);
            }, (error) => {
              this.loggedIn = false;
              localStorage.removeItem("authToken");
              resolve(this.loggedIn);
            });
          } else {
            resolve(false);
          }
        }, 800);
      }
    );
    return promise;
  }

  login(form: NgForm) {
    const formData = {
        "email": form.value.inputEmail,
        "password": form.value.inputPassword
      };
    this.http.post( this.global.baseUrl + "api/users/login" , formData).map((res) => res.json()
    ).subscribe((data) => {
      if (data.actionResult.success === true) {
        this.actionResult = data.actionResult;
        this.userService.setUser(data.user);
        this.loggedIn = true;
        localStorage.setItem('authToken', this.userService.getToken());
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  checkConnection(email: string, pass: string) {
    const formData = {
      "email": email,
      "password": pass
    };
    return this.http.post( this.global.baseUrl + "api/users/login" , formData).map((res) => res.json());
  }

  logout() {
    this.loggedIn = false;
    localStorage.removeItem("authToken");
    this.router.navigate(['/login']);
  }


}
