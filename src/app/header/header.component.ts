import { Component, OnInit } from '@angular/core';
import {AuthServiceService} from "../auth-service/auth-service.service";
import {Router} from "@angular/router";
import {UserService} from "../user/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private authService: AuthServiceService) { }

  ngOnInit() {

  }


  logout() {
    this.authService.logout();
  }


}
