import { Component, OnInit } from '@angular/core';
import {AuthServiceService} from "../auth-service/auth-service.service";
import {Http, RequestOptions, Headers} from "@angular/http";
import {UserService} from "../user/user.service";
import {Global} from "../global";
import {Router} from "@angular/router";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects = [];
  constructor(private router: Router, private http: Http, private global: Global, private  userService: UserService) { }

  ngOnInit() {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('x-projectstatus-token', localStorage.getItem('authToken'));
    const options = new RequestOptions();
    options.headers = myHeaders;
    this.http.get( this.global.baseUrl + "api/projects/get-all-project", options).map((res) => res.json()).subscribe((data) => {
      this.projects = data;
      console.log(this.projects);
    }, (error) => {

    });
  }

}
