import { Injectable } from '@angular/core';
import {User} from "../model/user.model";

@Injectable()
export class UserService {
  private user: User;
  constructor() { }

  setUser(user: User) {
    this.user = user;
  }

  getUser() {
    console.log(this.user);
    return this.user;
  }

  getToken() {
    return this.user.authToken;
  }
}
