import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions, Headers} from "@angular/http";
import {Global} from "../../global";
import {Project} from "../../model/project.model";

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css']
})
export class ReportDetailComponent implements OnInit {

  project: Project;
  activatedProjectId = this.route.snapshot.params.id;
  constructor(private route: ActivatedRoute, private router: Router, private http: Http, private global: Global) { }

  ngOnInit() {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('x-projectstatus-token', localStorage.getItem('authToken'));
    const options = new RequestOptions();
    options.headers = myHeaders;
    this.http.get( this.global.baseUrl + "api/projects/" + this.activatedProjectId + "/detail").map((res) => res.json()
    ).subscribe((data) => {
      if (data) {
        this.project = data;
      } else {
        this.router.navigate(['/login']);
      }
    });
  }


}
