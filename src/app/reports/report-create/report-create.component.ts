import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions, Headers} from "@angular/http";
import {Global} from "../../global";
import {UserService} from "../../user/user.service";
import {NgForm} from "@angular/forms";
import swal from 'sweetalert2';

@Component({
  selector: 'app-report-create',
  templateUrl: './report-create.component.html',
  styleUrls: ['./report-create.component.css']
})
export class ReportCreateComponent implements OnInit {
  @ViewChild("f") reportForm: NgForm;
  project;
  report;
  updateMessage;
  activatedProjectId = this.route.snapshot.params.id;
  activatedReportId = this.route.snapshot.params.rid;

  constructor(private route: ActivatedRoute, private router: Router, private http: Http, private global: Global, private  userService: UserService) {
  }

  ngOnInit() {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('x-projectstatus-token', localStorage.getItem('authToken'));
    const options = new RequestOptions();
    options.headers = myHeaders;
    this.http.get( this.global.baseUrl + "api/projects/" + this.activatedProjectId + "/detail", options).map((res) => res.json()
    ).subscribe((data) => {
      if (data) {
        this.project = data;
      } else {
        this.router.navigate(['/login']);
      }
    });

    this.http.get( this.global.baseUrl + "api/reports/" + this.activatedReportId + "/detail", options).map((res) => res.json()
    ).subscribe((data) => {
      console.log(data);
      if (data) {
        this.report = data;
        this.reportForm.setValue({
          projectCompletedRate: this.report.projectCompletedRate,
          content: this.report.content,
          projectIssues: this.report.projectIssues

        });
      } else {
        console.log(123);
        this.router.navigate(['/login']);
      }
    });
  }

  reportUpdateCreate(form: NgForm) {

    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('x-projectstatus-token', localStorage.getItem('authToken'));
    const options = new RequestOptions();
    options.headers = myHeaders;

    const formData = {
      "id": this.activatedReportId,
      "projectId": this.activatedProjectId,
      "content": form.value.content,
      "projectCompletedRate": form.value.projectCompletedRate,
      "projectIssues": form.value.projectIssues,
    };
    console.log(formData);
    this.http.post( this.global.baseUrl + "api/reports/update" , formData, options).map((res) => res.json()
    ).subscribe((data) => {
      swal({
        title: data.message,
        type: data.success === true ? 'success' : 'warning',
        confirmButtonColor: '#5CB86E',
        confirmButtonText: "Tamam"
      }).then(
        function () {
        },
        function (dismiss) {
        }
      );
    });
  }

}
